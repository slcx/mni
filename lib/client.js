const net = require('net')

const log = require('./log')

module.exports = class Client {
  constructor (port) {
    this.port = port
    this.sock = net.createConnection(this.port)

    this.registerHandlers()
  }

  disconnect () {
    log.verbose('disconnecting')
    this.sock.end()
    this.sock.destroy()
  }

  registerHandlers () {
    this.sock.on('close', (error) => {
      if (error) {
        log.error('connection closed due to error')
      } else {
        log.verbose('connection closed')
      }
    })

    this.sock.on('connect', () => {
      log.verbose('established connection')
    })

    this.sock.on('ready', () => {
      log.info('connection ready')
    })
  }

  send (packet) {
    this.sock.write(JSON.stringify(packet))
  }

  waitForData () {
    return new Promise((resolve, reject) => {
      const handler = (packet) => {
        resolve(JSON.parse(packet))
      }

      const errorHandler = (error) => {
        reject(error)
      }

      this.sock.once('data', handler)
      this.sock.once('error', errorHandler)
    })
  }

  async request (pkg) {
    log.verbose(`sending request for "${pkg}"`)
    this.send({ action: 'REQUEST_INSTALL', package: pkg })
    return await this.waitForData()
  }
}
