const Discord = require('discord.js')
const fetch = require('node-fetch')

const log = require('./log')

module.exports = class Approval {
  constructor (token, config) {
    this.config = config
    this.token = token
    this.client = new Discord.Client({
      disableEveryone: true,
      disabledEvents: ['TYPING_START', 'MESSAGE_CREATE', 'MESSAGE_UPDATE'],
    })

    this.client.on('debug', log.debug)
    this.client.on('verbose', log.verbose)
    this.client.on('warn', log.warn)
  }

  async getPackageInfo (pkg) {
    log.verbose(`querying "${pkg}" on the npm registry`)
    const body = await fetch(`https://registry.npmjs.org/${pkg}`)
      .then((resp) => resp.json())
    return body
  }

  async requestApproval (pkg) {
    const { approvalChannel, approvers } = this.config
    const channel = this.client.channels.get(approvalChannel)

    const info = await this.getPackageInfo(pkg)

    if (info == null || info.error) {
      const { error: msg = '<none>' } = info
      throw new Error(`Unable to locate package "${pkg}" (${msg})`)
    }

    const { latest } = info['dist-tags']
    const version = info.versions[latest]
    const { dependencies } = version

    const embed = new Discord.RichEmbed()
    embed.setTitle(pkg)
    embed.setDescription(info.description)
    embed.addField('Version', latest)

    if (Object.keys(dependencies).length !== 0) {
      embed.addField(
        'Dependencies',
        Object.entries(dependencies)
          .map(([name, version]) => `${name}@${version}`)
          .join(', ')
      )
    }

    log.info(`sending embed to ${approvalChannel}`)
    const msg = await channel.send(
      'Package installation request:',
      embed
    )

    await msg.react('👌')

    log.verbose('waiting for reactions')
    const reactions = await msg.awaitReactions(
      (reaction, user) => reaction.emoji.name === '👌' && approvers.includes(user.id),
      { time: 15000, max: 1 }
    )

    const reaction = reactions.first()
    const approver = reaction.users.filter((user) => !user.bot).first()

    log.info(`${approver.id} has approved the package install`)
    await msg.edit(
      `${approver.tag} (${approver.id}) has approved the installation of "${pkg}".`,
      { embed: null },
    )

    try {
      await msg.clearReactions()
    } catch (err) {
      // nope
    }

    return true
  }

  boot () {
    log.verbose('booting discord bot')
    this.client.login(this.token)
  }
}
